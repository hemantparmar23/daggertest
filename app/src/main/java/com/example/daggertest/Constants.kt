package com.example.daggertest

const val USER_PREFERENCES_NAME = "user_preferences"
const val BASE_URL = "https://stagingapi.momspresso.com/"
const val OTP_REGEX = "(?<!\\d)\\d{6}(?!\\d)"
const val INSTAGRAM_TAG = "1"
const val FACEBOOK_TAG = "2"
const val WHATSAPP_TAG = "3"
const val ALL_TAG = "4"
const val START_PAGE_INDEX = 1
const val PREF_FILE_NAME = "mymoney_pref"