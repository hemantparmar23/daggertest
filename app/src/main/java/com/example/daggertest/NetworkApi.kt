package com.example.daggertest

import okhttp3.ResponseBody
import retrofit2.http.GET

interface NetworkApi {

    @GET("/rewards/v3/campaigns/recommendations/")
    suspend fun getCampaign(): ResponseBody

    @GET("/rewards/v3/campaigns/recommendations/")
    suspend fun getCampaignHEAder(): ResponseBody
}