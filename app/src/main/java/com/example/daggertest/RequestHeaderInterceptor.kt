package com.example.daggertest

import android.os.Build
import android.util.Log
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RequestHeaderInterceptor @Inject constructor(
) :
    Interceptor {
    var userId: String = ""
    var token: String = ""
    var count: Int = 0

    fun testMethod() {
        Log.e("userId ", "userId = " + userId)
        Log.e("count ", "count = " + count)
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val requestBuilder: Request.Builder = request.newBuilder()
        requestBuilder.header("Accept-Language", Locale.getDefault().language)
        requestBuilder.addHeader("id", "" + userId)
        requestBuilder.addHeader("mc4kToken", token)
        requestBuilder.addHeader("agent", "android")
        requestBuilder.addHeader("manufacturer", Build.MANUFACTURER)
        requestBuilder.addHeader("model", Build.MODEL)
        requestBuilder.addHeader("source", "2")
        requestBuilder.addHeader("app_source", "mymoney")
        return chain.proceed(requestBuilder.build())
    }
}