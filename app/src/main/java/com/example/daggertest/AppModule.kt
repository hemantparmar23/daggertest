package com.example.daggertest

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.createDataStore
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.momspresso.mymoney.util.SingleToArrayAdapter
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton


@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class APPVERSION

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class REQUEST_HEADER_INTERCEPTOR

@Module
class AppModule {

    @Singleton
    @Provides
    fun providesRetrofitInstance(
        okHttpClient: OkHttpClient,
        moshiConverterFactory: MoshiConverterFactory
    ): Retrofit {
        return Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(moshiConverterFactory)
            .client(
                okHttpClient
            ).build()
    }

    @Singleton
    @Provides
    fun providesMoshiConverterFactory(): MoshiConverterFactory {
        val moshi = Moshi.Builder().add(SingleToArrayAdapter.INSTANCE).build()
        return MoshiConverterFactory.create(moshi)
    }

    @Singleton
    @Provides
    fun provideRequestOptions(): RequestOptions {
        return RequestOptions.placeholderOf(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
    }

    @Singleton
    @Provides
    fun providesGlideInstance(
        application: Application,
        requestOptions: RequestOptions
    ): RequestManager {
        return Glide.with(application).setDefaultRequestOptions(requestOptions)
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(
        interceptor: HttpLoggingInterceptor,
        @REQUEST_HEADER_INTERCEPTOR requestHeaderInterceptor: Interceptor
    ): OkHttpClient =
        OkHttpClient.Builder().addInterceptor(requestHeaderInterceptor).addInterceptor(interceptor)
            .addNetworkInterceptor(
                StethoInterceptor()
            ).build()

    @Singleton
    @Provides
    fun provideLoggingInterceptor() =
        HttpLoggingInterceptor().apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        }

    @Singleton
    @Provides
    @REQUEST_HEADER_INTERCEPTOR
    fun provideRequestHeaderInterceptor(): Interceptor {
        return RequestHeaderInterceptor()
    }

    @Singleton
    @Provides
    @APPVERSION
    fun providesAppVersion(
        application: Application
    ): String {
        var packageInfo: PackageInfo? = null
        try {
            packageInfo = application.packageManager.getPackageInfo(application.packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return when (packageInfo) {
            null -> {
                ""
            }
            else -> {
                packageInfo.versionName
            }
        }
    }

    @Singleton
    @Provides
    fun providesDatastore(application: Application): DataStore<Preferences> {
        return application.createDataStore(name = USER_PREFERENCES_NAME)
    }

    @Provides
    @Singleton
    fun provideSharedPreference(application: Application): SharedPreferences {
        return application.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideNetworkApi(retrofit: Retrofit): NetworkApi {
        return retrofit.create(NetworkApi::class.java)
    }

}