package com.example.daggertest

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var networkApi: NetworkApi


    @Inject
    lateinit var requestHeaderInterceptor: RequestHeaderInterceptor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (findViewById<TextView>(R.id.call1)).setOnClickListener {
            try {
                requestHeaderInterceptor.count = 3
                requestHeaderInterceptor.userId = "vfvfvfv"
                requestHeaderInterceptor.token = "vvvvv"
                requestHeaderInterceptor.testMethod()
                CoroutineScope(Dispatchers.IO).launch {
                    networkApi.getCampaign()
                }
            } catch (e: Exception) {
                Log.e("dwadwa", Log.getStackTraceString(e))
            }
            //            networkApi.getCampaign()

        }

        (findViewById<TextView>(R.id.call2)).setOnClickListener {
            requestHeaderInterceptor.count = 3
            requestHeaderInterceptor.userId = "vfvfvfv"
            requestHeaderInterceptor.token = "vvvvv"
            requestHeaderInterceptor.testMethod()
            CoroutineScope(Dispatchers.IO).launch {
                networkApi.getCampaignHEAder()
            }
        }
    }
}