package com.example.daggertest

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(
    )
    abstract fun contributeLoginActivity(): MainActivity

}